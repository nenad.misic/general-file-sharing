#MALI PROBLEM
#MOJ HOG JE OTKUCAN DA PODRAZUMEVA DA POSTOJE DVE KLASE (POZITIVNA I NEGATIVNA)
#NISAM SIGURAN DAL MOZE DA SE NAKUCA ZA VISE KLASA, AL ISTRAZI


#expects two parameters, lists of positive and negative images
#returns tuple (numpy.vstack containing positive and negative features, numpy.array containing labels) 
def train_hog(pos_imgs, neg_imgs):
    pos_features = []
    neg_features = []
    labels = []

    nbins = 9 
    cell_size = (8, 8)
    block_size = (3, 3)

    hog = cv2.HOGDescriptor(_winSize=(100 // cell_size[1] * cell_size[1], 
                                      100 // cell_size[0] * cell_size[0]),
                            _blockSize=(block_size[1] * cell_size[1],
                                        block_size[0] * cell_size[0]),
                            _blockStride=(cell_size[1], cell_size[0]),
                            _cellSize=(cell_size[1], cell_size[0]),
                            _nbins=nbins)

    for img in pos_imgs:
        pos_features.append(hog.compute(img))
        labels.append(1)

    for img in neg_imgs:
        neg_features.append(hog.compute(img))
        labels.append(0)

    pos_features = np.array(pos_features)
    neg_features = np.array(neg_features)
    x = np.vstack((pos_features, neg_features))
    y = np.array(labels)
    
    return x, y

pos_imgs, neg_imgs = #ucitaj svoje slike (pozitivne i negativne)
x, y = train_hog(pos_imgs, neg_imgs)

#split the dataset into train and test sets
x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=0.1, random_state=42)

#reshape them for sklearn
x_train = reshape_data(x_train)
x_test = reshape_data(x_test)



#TRENIRANJE KNN

#initialize KNN classifier
clf_knn = KNeighborsClassifier(n_neighbors=10)

#train the classifier using HOG features
clf_knn = clf_knn.fit(x_train, y_train)

#predict train and test data to extract accuracy
y_train_pred = clf_knn.predict(x_train)
y_test_pred = clf_knn.predict(x_test)

#print train and test accuracy
print("Train accuracy: ", accuracy_score(y_train, y_train_pred))
print("Validation accuracy: ", accuracy_score(y_test, y_test_pred))





# TRENIRANJE SVM

#initialize SVM classifier
clf_svm = SVC(kernel='linear', probability=True) 

#train the classifier using HOG features
clf_svm.fit(x_train, y_train)

#predict train and test data to extract accuracy
y_train_pred = clf_svm.predict(x_train)
y_test_pred = clf_svm.predict(x_test)

#print train and test accuracy
print("Train accuracy: ", accuracy_score(y_train, y_train_pred))
print("Validation accuracy: ", accuracy_score(y_test, y_test_pred))