# Ovo je moj model, proguglaj sta koji sloj znači, pa prilagodi svom problemu
# Gledaj da ne bude ista mreža da ne bismo popadali na plagijatima
def create_model():
    model = Sequential()
    model.add(Conv2D(32, (3, 3), input_shape=(100,100,1))) #100,100,1 je dimenzija moje ulazne slike 100x100 pixela, grayscale. Tebi ce trebati (nesto, nesto, 3) (nesto nesto je dimenzija tvoje slike)
    model.add(Activation('sigmoid'))
    model.add(MaxPooling2D(pool_size=(2, 2)))

    model.add(Conv2D(32, (3, 3)))
    model.add(Activation('relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))

    # 3D feature mape do sad

    model.add(Flatten())  # konverzija u 1D
    model.add(Dense(32))
    model.add(Activation('sigmoid'))
    model.add(Dropout(0.5))
    model.add(Dense(2))
    model.add(Activation('sigmoid'))

    model.compile(loss= # 'binary_crossentropy' ili neki drugi (tebi treba neki drugi jer imas vise klasa),
                  optimizer= # 'adam', 'rmsprop' ... ,
                  metrics=['accuracy'])
    
    return model

def train_model():
    import matplotlib.pyplot as plt
    import numpy as np
    import argparse
    import random
    import cv2
    import os
    import matplotlib
    matplotlib.use("Agg")

    num_epochs = 5
    batch_size = 32
    model = create_model()

    checkpointer = ModelCheckpoint(filepath="best_weights_cnn.hdf5", verbose=1, save_best_only=True)

    images = []
    labels = []

    #iteriraj kroz slike, guraj slike u images a labelu (cvet_jedan, cvet_dva...) u labels, da im se poklapa redosled
    #ja sam trpao grayscale slike, ali tebi verovatno treba u boji, zato sto ti boja kod cvetova ima značenje
    #to će ti promeniti i arhitekturu mreže jer očekuje 3 matrice u slučaju slike u boji
    # ...
        
    images = np.array(images)
    labels = np.array(labels)

    (trainX, testX, trainY, testY) = train_test_split(images,labels, test_size=0.2, random_state=42)

    #num_classes moraš promeniti, jer kod tebe ima više klasa
    trainY = to_categorical(trainY, num_classes=2)
    testY = to_categorical(testY, num_classes=2)

    aug = ImageDataGenerator(rotation_range=10, width_shift_range=0.1,height_shift_range=0.1, shear_range=0.2, zoom_range=0.2,horizontal_flip=True, fill_mode="nearest")


    H = model.fit_generator(aug.flow(trainX, trainY, batch_size=batch_size),
                            validation_data=(testX, testY), steps_per_epoch=len(trainX) // batch_size,
                            epochs=num_epochs,callbacks=[checkpointer], verbose=1)

def load_model():
    model = create_model()
    model.load_weights("Classifiers/best_weights_cnn.hdf5")
    return model